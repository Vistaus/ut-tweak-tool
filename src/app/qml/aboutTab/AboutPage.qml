import QtQuick 2.4
import QtGraphicalEffects 1.0
import Ubuntu.Components 1.3
import "../components"

Page {
    id: aboutPage

    header: PageHeader {
        id: header
        title: i18n.tr("About")
        opacity: 1

        extension: Sections {
            id: sections
            anchors {
                horizontalCenter: parent.horizontalCenter
                bottom: parent.bottom
            }

            actions: [
                Action {
                    text: i18n.tr("About")
                },
                Action {
                    text: i18n.tr("Credits")
                }
            ]
            onSelectedIndexChanged: {
                tabView.currentIndex = selectedIndex
            }

        }
    }

    ListModel {
        id: creditsModel
        Component.onCompleted: initialize()

        function initialize() {
            // Resources
            creditsModel.append({ category: i18n.tr("Resources"), name: i18n.tr("Bugs"), link: "https://gitlab.com/myii/ut-tweak-tool/issues" })
            creditsModel.append({ category: i18n.tr("Resources"), name: i18n.tr("Contact"), link: "mailto:incoming+myii/ut-tweak-tool@incoming.gitlab.com" })
            creditsModel.append({ category: i18n.tr("Resources"), name: i18n.tr("Translations"), link: "https://gitlab.com/myii/ut-tweak-tool/tree/master/po"})

            // Developers
            creditsModel.append({ category: i18n.tr("Developers"), name: "Verzegnassi Stefano (" + i18n.tr("Founder") + ")", link: "https://launchpad.net/~verzegnassi-stefano" })
            creditsModel.append({ category: i18n.tr("Developers"), name: "Imran Iqbal (" + i18n.tr("Maintainer") + ")", link: "https://github.com/myii" })
            creditsModel.append({ category: i18n.tr("Developers"), name: "Rudi Timmermans (" + i18n.tr("Maintainer") + ")", link: "https://twitter.com/Xray20001" })
            creditsModel.append({ category: i18n.tr("Developers"), name: "Kugi Eusebio (" + i18n.tr("Contributor") + ")", link: "https://github.com/kugiigi" })
            creditsModel.append({ category: i18n.tr("Developers"), name: "Joan CiberSheep (" + i18n.tr("Contributor") + ")", link: "https://twitter.com/cibersheep" })
            creditsModel.append({ category: i18n.tr("Developers"), name: "Mateo Salta (" + i18n.tr("Contributor") + ")", link: "https://github.com/mateosalta" })
        }

    }

    VisualItemModel {
        id: tabs

        Item {
            id: aboutItem
            width: tabView.width
            height: tabView.height
            opacity: tabView.currentIndex == 0 ? 1 : 0

            Behavior on opacity {
                NumberAnimation {duration: 200; easing.type: Easing.InOutCubic }
            }

            Flickable {
                id: flickable
                anchors.fill: parent
                contentHeight: layout.height + units.gu(10)

                Column {
                    id: layout

                    spacing: units.gu(3)
                    anchors {
                        top: parent.top
                        left: parent.left
                        right: parent.right
                        topMargin: units.gu(5)
                    }

                    UbuntuShape {
                        anchors.horizontalCenter: parent.horizontalCenter
                        height: width
                        width: Math.min(parent.width/2, parent.height/2)
                        source: Image {
                            source: Qt.resolvedUrl("../../ut-tweak-tool.png")
                        }
                        radius: "large"
                    }

                    Column {
                        width: parent.width
                        Label {
                            width: parent.width
                            textSize: Label.XLarge
                            font.weight: Font.DemiBold
                            horizontalAlignment: Text.AlignHCenter
                            wrapMode: Text.WordWrap
                            text: i18n.tr("Ubuntu Touch Tweak Tool")
                        }
                        Label {
                            width: parent.width
                            horizontalAlignment: Text.AlignHCenter
                            text: i18n.tr("Version %1").arg(appVersion)
                        }
                    }

                    Label {
                        width: parent.width
                        wrapMode: Text.WordWrap
                        horizontalAlignment: Text.AlignHCenter
                        text: i18n.tr("A Tweak tool application for Ubuntu Touch.<br/>Tweak your device, unlock its full power! Advanced settings for your ubuntu device.")
                    }

                    Column {
                        anchors {
                            left: parent.left
                            right: parent.right
                            margins: units.gu(2)
                        }

                        Label {
                            width: parent.width
                            wrapMode: Text.WordWrap
                            horizontalAlignment: Text.AlignHCenter
                            text: i18n.tr("A special thanks to all translators, beta-testers, and users<br/>in general who improve this app with their work and feedback<br/>")
                        }

                        Label {
                            width: parent.width
                            wrapMode: Text.WordWrap
                            horizontalAlignment: Text.AlignHCenter
                            text: i18n.tr("(C) 2014-2016 Stefano Verzegnassi<br/>Maintainer (C) 2018 Rudi Timmermans<br/>") +
                                i18n.tr("Maintainer (C) 2018-2020 Imran Iqbal<br/>")
                        }

                        Label {
                            textSize: Label.Small
                            width: parent.width
                            wrapMode: Text.WordWrap
                            horizontalAlignment: Text.AlignHCenter
                            text: i18n.tr("Released under the terms of the GNU GPL v3")
                        }
                    }

                    Label {
                        width: parent.width
                        wrapMode: Text.WordWrap
                        textSize: Label.Small
                        horizontalAlignment: Text.AlignHCenter
                        linkColor: theme.palette.normal.focus
                        text: i18n.tr("Source code available on %1").arg("<a href=\"https://gitlab.com/myii/ut-tweak-tool\">GitLab</a>")
                        onLinkActivated: Qt.openUrlExternally(link)
                    }

                }
            }
        }

        Item {
            id: creditsItem
            width: tabView.width
            height: tabView.height
            opacity: tabView.currentIndex == 1 ? 1 : 0

            Behavior on opacity {
                NumberAnimation {duration: 200; easing.type: Easing.InOutCubic }
            }

            ListView {
                id: creditsListView

                model: creditsModel
                anchors.fill: parent
                section.property: "category"
                section.criteria: ViewSection.FullString
                section.delegate: ListItemHeader {
                    title: section
                }

                delegate: ListItem {
                    height: creditsDelegateLayout.height
                    divider.visible: false
                    ListItemLayout {
                        id: creditsDelegateLayout
                        title.text: model.name
                        ProgressionSlot {}
                    }
                    onClicked: Qt.openUrlExternally(model.link)
                }
            }

        }
    }

    ListView {
        id: tabView
        anchors {
            top: aboutPage.header.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        model: tabs
        currentIndex: 0

        orientation: Qt.Horizontal
        snapMode: ListView.SnapOneItem
        highlightRangeMode: ListView.StrictlyEnforceRange
        highlightMoveDuration: UbuntuAnimation.FastDuration

        onCurrentIndexChanged: {
            sections.selectedIndex = currentIndex
        }

    }
}

