/*
  This file is part of ut-tweak-tool
  Copyright (C) 2015 Stefano Verzegnassi

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License 3 as published by
  the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see http://www.gnu.org/licenses/.
*/

import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import StorageManager 1.0
import GSettings 1.0
import Ubuntu.SystemSettings.Sound 1.0
import TweakTool 1.0

import "../components"
import "../components/ListItems" as ListItems
import "../components/Dialogs"
import "../js/shell.js" as Shell

// TODO: Show an audio trimmer when a file is picked
// TODO: Always create a new copy of the file in the internal storage.

Page {
    id: rootItem

    signal filePicked(var path)

    readonly property string prefixName: "ut_tweak_"
    readonly property string defaultPath: "/usr/share/sounds/ubuntu/notifications/"

    onFilePicked:{
        console.log(path)
    }

    header: PageHeader {
        title: i18n.tr("Notification sound")
        flickable: view.flickableItem
    }

    ScrollView {
        id: view
        anchors.fill: parent

        Column {
            id: column

            width: view.width

            function getSettings() {
                messageSoundItem.subtitle.text = StorageManager.getFileFullName(settings.incomingMessageSound).replace(prefixName, "").replace(/_/g, " ")
            }

            function setIncomingMessageSound(file, isCustom) {
                // Check initial mount status
                var outputStatus = Shell.cmdMountGetRoRwStatus();
                var rwActive = outputStatus.indexOf("rw") > -1;

                // Only remount if `rw` wasn't initially active
                if (!rwActive) {
                    //Make rootfs temporarily rewritable
                    Shell.cmdMountSetRoRwStatus('rw');
                }

                //Delete custom audio files
                Process.launch('/bin/sh -c "echo ' + pamLoader.item.password + ' | sudo -S rm ' + defaultPath + prefixName + "*")

                if(isCustom){
                    var customFullPath = defaultPath + prefixName + StorageManager.getFileFullName(file).replace(/ /g, "_")

                    //Create the symlink of the file
                    Process.launch('/bin/sh -c "echo ' + pamLoader.item.password + " | sudo -S ln -s '" + file + "' " + customFullPath + '"')

                    //Copy custom audio file to sound directory
                    //Process.launch('/bin/sh -c "echo ' + pamLoader.item.password + " | sudo -S cp '" + file + "' " + customFullPath + '"')
                    //Process.launch('/bin/sh -c "echo ' + pamLoader.item.password + ' | sudo -S chown root:root ' + customFullPath)
                    //Process.launch('/bin/sh -c "echo ' + pamLoader.item.password + ' | sudo -S chmod go=r ' + customFullPath)

                    settings.incomingMessageSound = customFullPath;
                    backend.incomingMessageSound = customFullPath;
                }else{
                    settings.incomingMessageSound = file;
                    backend.incomingMessageSound = file;
                }

                // Only remount if `rw` wasn't initially active
                if (!rwActive) {
                    //Revert back rootfs to read-only
                    Shell.cmdMountSetRoRwStatus('ro');
                }

                getSettings();
            }

            Component.onCompleted: getSettings()

            ListItems.Warning {
                text: i18n.tr("Please note that if you choose an audio file from an external media (e.g. SD card), that file could not be played if the media will be removed.")
            }

            ListItems.SectionDivider { text: i18n.tr("Message received") }

            ListItems.Warning {
                iconName: "messages-new"
                text: i18n.tr("Ensure your audio file length is max. 10 seconds.<br><b>N.B.</b> The file will be played until its end: if you use a 3 minutes song, your device will be ringing for that time!")
            }

            ListItems.Warning {
                text: i18n.tr("This feature needs to create a file inside your system (/usr/share/sounds/ubuntu/notifications) to avoid breaking notification sound from other applications such as Telegram and FluffyChat.<br><b>N.B.</b> Please, note that it will be removed after every system update, you'll need to set it up again.")
            }

            ListItems.Button {
                id: messageSoundItem

                title.text: i18n.tr("Current notification sound:")
                button {
                    text: i18n.tr("Pick")
                    onClicked: {
                        pageStack.addPageToCurrentColumn(rootItem, fileDialog)
                    }
                }
            }

            ListItems.Base {
                Label {
                    text: i18n.tr("Restore default...")
                    anchors.verticalCenter: parent.verticalCenter
                }

                onClicked: {
                    var dialog = PopupUtils.open(defaultDialog);
                    dialog.accepted.connect(function() {
                        column.setIncomingMessageSound("/usr/share/sounds/ubuntu/notifications/Xylo.ogg", false)
                    })
                }
            }

            GSettings {
                id: settings
                schema.id: "com.ubuntu.touch.sound"
            }

            UbuntuSoundPanel { id: backend }

            Component {
                id: fileDialog

                FilePickerPage {
                    id: pickerPage

                    property var callback: function(){}

                    title: i18n.tr("Choose a notification sound")
                    folder: "file://" + StorageManager.getXdgFolder(StorageManager.HomeLocation)
                    nameFilters: ["*.mp3", "*.ogg"]
                    onAccepted: {
                        column.setIncomingMessageSound(path.toString().replace("file://", ""), true)
                        rootItem.pageStack.removePages(pickerPage)

                        var regEx = /\.(mp3)$/i

                        if(regEx.test(path.toString())){
                            var dialog = PopupUtils.open(mp3WarnDialog);
                        }
                    }
                }
            }

            Component {
                id: defaultDialog

                Dialog {
                    id: defaultDialogue

                    signal accepted

                    title: i18n.tr("Restore default")
                    text: i18n.tr("Are you sure?")

                    Button {
                        text: i18n.tr("Restore")
                        color: theme.palette.normal.negative
                        onClicked: {
                            defaultDialogue.accepted();
                            PopupUtils.close(defaultDialogue);
                        }
                    }

                    Button {
                        text: i18n.tr("Cancel")

                        onClicked: PopupUtils.close(defaultDialogue);
                    }
                }
            }

            Component {
                id: mp3WarnDialog

                Dialog {
                    id: mp3WarnDialogue

                    signal accepted

                    title: i18n.tr("MP3 File Selected")
                    text: i18n.tr("OGG format is highly recommended. Currently, MP3 format breaks the notification sound for other apps such as Telegram and FluffyChat")

                    Button {
                        text: i18n.tr("OK")
                        onClicked: PopupUtils.close(mp3WarnDialogue);
                    }
                }
            }
        }
    }
}
