/*
  This file is part of ut-tweak-tool
  Copyright (C) 2015 Stefano Verzegnassi

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License 3 as published by
  the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see http://www.gnu.org/licenses/.
*/

import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import TweakTool 1.0
import QtQuick.Layouts 1.1

import "../components"
import "../components/ListItems" as ListItems

Page {
    id: rootItem

    header: PageHeader {
        title: i18n.tr("SSH settings")
        flickable: view.flickableItem
    }

    ScrollView {
        id: view
        anchors.fill: parent

        Column {
            width: view.width

            ListItems.Warning {
                iconName: "info"
                text: "%1%2%3%4%5%6"
                    .arg(i18n.tr("For information on how to configure SSH, refer back to the UBports documentation:"))
                    .arg("<p><a href='http://docs.ubports.com/")
                    // TRANSLATORS: Select the best available language for http://docs.ubports.com
                    .arg(i18n.tr("en"))
                    .arg("/latest/userguide/advanceduse/ssh.html'>")
                    // TRANSLATORS: Use the documentation's page title for the selected language
                    .arg(i18n.tr("Shell access via ssh"))
                    .arg("</a></p>")
            }

            ListItems.SectionDivider { text: i18n.tr("Available settings") }

            ListItems.Control {
                title.text: i18n.tr("Enable SSH access")

                Switch {
                    checked: Process.launch("android-gadget-service status ssh").indexOf("ssh enabled") > -1;
                    onClicked: {
                        if (checked) {
                            Process.launch("android-gadget-service enable ssh");
                        } else {
                            Process.launch("android-gadget-service disable ssh");
                        }
                    }
                }
            }
        }
    }
}
